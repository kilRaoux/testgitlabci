# Intégration continue avec GitLab


## prérequis
Posséder ou creer un compte gitlab.
Installer git sur votre machine.

## description

__Gitlab__ : est un repository git.

__Runner ou Worker__ : est un worker. Il joue permet un traitement sur le code et renvois les resultas a gitlab.  
Par exemple: *il peut lancer les testes unitaires et renvoier leurs resultas*.

le but : 
1. Mettre en place une integration continue avec gitlab.
2. Mettre en place des runners ou workers.


## Lancer le projet

1. Dans Gitlab creer un nouveau projet:
2. Dans le dossier de votre choix cloner votre gitlab:
```
git clone <URL de votre repository git>
```

3. Aller dans un browser et taper:
```
http://localhost:9080
```
4. Se Logger.

## Liens utiles


## configuration spécifique

## Infos sur `gitlab-ci.yml`
